# README #

### What is this repository for? ###

* Repository is designed to have some basic graph algorithms defined for C#.

### How do I get set up? ###

* Install the NUnit test runners (Visual Studio plugin). Other than that, should be up and running to use tests.

### Contribution guidelines ###

* Currently using NUnit for unit tests. If it comes up will use NSubstitute for mocking.

### Usage ###
* For example usage, look at the unit tests.

### Who do I talk to? ###

* Repo owner or admin