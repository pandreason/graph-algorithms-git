﻿using System;
using System.Collections.Generic;

namespace GraphAlgorithms
{
    public class BreadthFirstSearch
    {
        public List<Node<T>> FindAllNodesConnectedToGivenNode<T>(List<Node<T>> graph, Node<T> nodeToStartWith)
        {
            if (graph == null)
                throw new ArgumentNullException("graph");
            if (nodeToStartWith == null)
                throw new ArgumentNullException("nodeToStartWith");

            var nodeQueue = new Queue<Node<T>>();
            var nodesInConnectedPart = new List<Node<T>>();
            nodeQueue.Enqueue(nodeToStartWith);
            nodesInConnectedPart.Add(nodeToStartWith);

            while (nodeQueue.Count != 0)
            {
                var tempNode = nodeQueue.Dequeue();
                var nodesConnectedToThisNode = tempNode.Neighbors;

                foreach (var node in nodesConnectedToThisNode)
                {
                    if (nodesInConnectedPart.Contains(node)) continue;

                    nodesInConnectedPart.Add(node);
                    nodeQueue.Enqueue(node);
                }
            }

            return nodesInConnectedPart;
        }
    }
}
