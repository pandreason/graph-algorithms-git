﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GraphAlgorithms
{
    public class DepthFirstSearch
    {
        /// <summary>
        /// Checks a graph to see if a cycle has been detected.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="graph">The list of all the nodes to be processed.</param>
        /// <returns>True if there is a cycle, false otherwise</returns>
        public bool IsThereACyle<T>(List<Node<T>> graph) 
        {
            if (graph == null) throw new ArgumentNullException("graph");

            foreach (var node in graph)
            {
                if (HasACycleBeenDetected(node, new List<Node<T>>()))
                {
                    return true;
                }
            }
            return false;
        }

        private bool HasACycleBeenDetected<T>(Node<T> node, List<Node<T>> visitedNodes)
        {
            if (visitedNodes.Contains(node))
            {
                return true;
            }
            visitedNodes.Add(node);
            foreach (var neighbor in node.Neighbors)
            {
                return HasACycleBeenDetected(neighbor, new List<Node<T>>(visitedNodes));
            }
            return false;
        }

        /// <summary>
        /// Returns the number of connections between two nodes.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="startingNode">The starting point of the graph.</param>
        /// <param name="finishNode">The finish point of the graph</param>
        /// <returns>
        ///     The number of connections between the two nodes.
        ///     If there is a cycle such that it cannot be determined, Int32.MinValue is returned.
        ///     If the startingNode and finishNode are not connected at all, then -1 is returned
        /// </returns>
        public int DegreesOfSeparation<T>(Node<T> startingNode, Node<T> finishNode)
        {
            if (startingNode == null)
            {
                throw new ArgumentNullException("startingNode");
            }
            if (finishNode == null)
            {
                throw new ArgumentNullException("finishNode");
            }
            return FindNeighborLength(startingNode, finishNode, new List<Node<T>>());
        }

        private int FindNeighborLength<T>(Node<T> startingNode, Node<T> finishNode, List<Node<T>> visitedNodes)
        {
            if (startingNode == finishNode)
            {
                return 0;
            }
            if (visitedNodes.Contains(startingNode))
            {
                return int.MinValue;
            }

            visitedNodes.Add(startingNode);
            foreach (var neighbor in startingNode.Neighbors)
            {
                var neighborCount = FindNeighborLength<T>(neighbor, finishNode, new List<Node<T>>(visitedNodes));
                if (neighborCount < 0)
                {
                    return neighborCount;
                }
                return 1 + neighborCount;
            }
            return -1;
        }


        /// <summary>
        /// Returns a list of nodes that will traverse from a starting node to a finish node.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="startingNode">The node to start with</param>
        /// <param name="finishNode">The node to find</param>
        /// <returns>
        ///     A list of which nodes to traverse to get from the start node to the finish node. 
        ///     If the startingNode is not connected in any way or if there is a loop that prevents
        ///     from finding the finish node, an empty list is returned.
        /// </returns>
        public List<Node<T>> GetPathBetweenNodes<T>(Node<T> startingNode, Node<T> finishNode)
        {
            if (startingNode == null) throw new ArgumentNullException("startingNode");
            if (finishNode == null) throw new ArgumentNullException("finishNode");

            return FindNeighborPath(startingNode, finishNode, new List<Node<T>>());
        }

        private List<Node<T>> FindNeighborPath<T>(Node<T> currentNode, Node<T> finishNode, List<Node<T>> visitedNodes)
        {
            if (currentNode == finishNode)
            {
                return new List<Node<T>>{currentNode};
            }
            if (visitedNodes.Contains(currentNode))
            {
                return new List<Node<T>>();
            }
            visitedNodes.Add(currentNode);
            foreach (var neighbor in currentNode.Neighbors)
            {
                var nextPath = FindNeighborPath(neighbor, finishNode, new List<Node<T>>(visitedNodes));
                if (!nextPath.Any())
                {
                    return nextPath;
                }
                var results = new List<Node<T>> {currentNode};
                results.AddRange(nextPath);
                return results;
            }
            return new List<Node<T>>();
        }
    }
}
