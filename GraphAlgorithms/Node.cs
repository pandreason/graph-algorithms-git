﻿using System;
using System.Collections.Generic;

namespace GraphAlgorithms
{
    public class Node<T> 
    {
        public List<Node<T>> Neighbors { get; private set; }

        public T Data { get; private set; }

        public Node(T data)
        {
            if (data == null) throw new ArgumentNullException("data");
            Data = data;
            Neighbors = new List<Node<T>>();
        }

        public void AddNeighbor(Node<T> node)
        {
            if (node == null) throw new ArgumentNullException("node");
            if (!Neighbors.Contains(node))
            {
                Neighbors.Add(node);
            }
        }
    }
}
