﻿using System;
using System.Collections.Generic;

namespace Graph
{
    public class Node<T> where T:class 
    {
        public List<Node<T>> Neighbors;

        public T Data { get; private set; }

        public Node(T data)
        {
            if (data == null) throw new ArgumentNullException("data");
            Data = data;
        }

        public void AddNeighbor(Node<T> node)
        {
            if (node == null) throw new ArgumentNullException("node");
            if (!Neighbors.Contains(node))
            {
                Neighbors.Add(node);
            }
        }
    }
}
