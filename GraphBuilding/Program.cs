﻿using System;
using System.Collections.Generic;
using System.IO;
using GraphAlgorithms;

namespace GraphBuilding
{
    class Program
    {
        static void Main(string[] args)
        {
            var aNode = new Node<string>("A");
            var bNode = new Node<string>("B");
            var cNode = new Node<string>("C");
            var dNode = new Node<string>("D");

            //aNode.AddNeighbor(bNode);
            //bNode.AddNeighbor(aNode);

            var depthFirst = new DepthFirstSearch();

            var graph = new List<Node<string>> {aNode, bNode, cNode, dNode};

            var result = depthFirst.IsThereACyle(graph);
            if (result)
            {
                Console.Write("There are loops.");
            }
            else
            {
                Console.Write("Everything is wrong.");
            }

            aNode.AddNeighbor(bNode);
            aNode.AddNeighbor(dNode);

            var degreesOfSeparation = depthFirst.DegreesOfSeparation(aNode, cNode);
            Console.WriteLine("There are " + degreesOfSeparation + " degrees of separation between Node A and Node C.");
            degreesOfSeparation = depthFirst.DegreesOfSeparation(aNode, bNode);
            Console.WriteLine("There are " + degreesOfSeparation + " degrees of separation between Node A and Node B.");
            Console.ReadLine();
        }
    }
}
