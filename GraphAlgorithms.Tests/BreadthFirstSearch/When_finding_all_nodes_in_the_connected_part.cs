﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace GraphAlgorithms.Tests.BreadthFirstSearch
{
    [TestFixture]

    // ReSharper disable once InconsistentNaming
    public class When_finding_all_nodes_in_the_connected_part
    {
        private GraphAlgorithms.BreadthFirstSearch _algorithm;

        [SetUp]
        public void Setup()
        {
            _algorithm = new GraphAlgorithms.BreadthFirstSearch();
        }
        
        [Test]
        public void And_the_node_to_start_with_is_null_then_an_exception_is_thrown()
        {
            Assert.Throws<ArgumentNullException>(
                () => _algorithm.FindAllNodesConnectedToGivenNode(new List<Node<int>>(), null));
        }

        [Test]
        public void And_the_graph_is_null_then_an_exception_is_thrown()
        {
            Assert.Throws<ArgumentNullException>(
                () => _algorithm.FindAllNodesConnectedToGivenNode(null, new Node<int>(2)));
        }

        [Test]
        public void And_the_graph_contains_only_a_single_node_then_the_connected_part_containing_only_that_node_is_returned()
        {
            var node1 = new Node<int>(1);
            var graph = new List<Node<int>> { node1 };

            var connectedPart = _algorithm.FindAllNodesConnectedToGivenNode(graph, node1);

            var expectedConnectedPart = graph;

            CollectionAssert.AreEqual(expectedConnectedPart, connectedPart);
        }

        [Test]
        public void And_the_graph_contains_two_nodes_1_to_2_then_the_connected_part_containing_A_and_B_are_returned()
        {
            var node1 = new Node<int>(1);
            var node2 = new Node<int>(2);

            node1.AddNeighbor(node2);
            var graph = new List<Node<int>>
            {
                node1,
                node2,
            };

            var connectedPart = _algorithm.FindAllNodesConnectedToGivenNode(graph, node1);

            var expectedConnectedPart = graph;

            CollectionAssert.AreEqual(expectedConnectedPart, connectedPart);
        }

        [Test]
        public void And_the_connected_part_has_two_nodes_and_there_are_three_nodes_in_the_graph_then_the_correct_connected_part_is_returned()
        {
            var node1 = new Node<int>(1);
            var node2 = new Node<int>(2);
            var node3 = new Node<int>(3);

            node1.AddNeighbor(node2);
            var graph = new List<Node<int>>
            {
                node1,
                node2,
                node3
            };

            var connectedPart = _algorithm.FindAllNodesConnectedToGivenNode(graph, node1);

            graph.Remove(node3);

            var expectedConnectedPart = graph;

            CollectionAssert.AreEqual(expectedConnectedPart, connectedPart);
        }

        [Test]
        public void And_the_starting_node_has_no_connections_then_the_starting_node_is_returned()
        {
            var node1 = new Node<int>(1);
            var node2 = new Node<int>(2);
            var node3 = new Node<int>(3);

            node1.AddNeighbor(node2);
            var graph = new List<Node<int>>
            {
                node1,
                node2,
                node3
            };

            var connectedPart = _algorithm.FindAllNodesConnectedToGivenNode(graph, node3);

            graph.Remove(node1);
            graph.Remove(node2);

            var expectedConectedPart = graph;

            CollectionAssert.AreEqual(expectedConectedPart, connectedPart);
        }

        [Test]
        public void And_the_connected_part_is_the_whole_graph_then_the_whole_graph_is_returned()
        {
            var node1 = new Node<int>(1);
            var node2 = new Node<int>(2);
            var node3 = new Node<int>(3);

            node1.AddNeighbor(node2);
            node1.AddNeighbor(node3);
            var graph = new List<Node<int>>
            {
                node1,
                node2,
                node3
            };

            var connectedPart = _algorithm.FindAllNodesConnectedToGivenNode(graph, node1);

            var expectedConectedPart = graph;

            CollectionAssert.AreEqual(expectedConectedPart, connectedPart);
        }

        [Test]
        //digraph is still communative
        public void And_the_connected_part_is_the_whole_graph_but_not_all_nodes_are_connected_to_the_source_node_then_the_whole_graph_is_returned()
        {
            var node1 = new Node<int>(1);
            var node2 = new Node<int>(2);
            var node3 = new Node<int>(3);
            var node4 = new Node<int>(4);
            var node5 = new Node<int>(5);

            node1.AddNeighbor(node2);
            node1.AddNeighbor(node3);

            node3.AddNeighbor(node4);
            node2.AddNeighbor(node5);

            var graph = new List<Node<int>>
            {
                node1,
                node2,
                node3,
                node5,
                node4,
            };

            var connectedPart = _algorithm.FindAllNodesConnectedToGivenNode(graph, node1);

            var expectedConectedPart = graph;

            CollectionAssert.AreEqual(expectedConectedPart, connectedPart);
        }
    }
}
