﻿using NUnit.Framework;
using System;

namespace GraphAlgorithms.Tests.DepthFirstSearch
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    class When_calculating_the_degrees_of_separation
    {
        private GraphAlgorithms.DepthFirstSearch _algorithm;

        [SetUp]
        public void Setup()
        {
            _algorithm = new GraphAlgorithms.DepthFirstSearch();
        }

        [Test]
        public void And_the_startingNode_is_null_then_an_exception_is_thrown()
        {
            var node = new Node<int>(2);

            Assert.Throws<ArgumentNullException>(() => _algorithm.DegreesOfSeparation(null, node));
        }

        [Test]
        public void And_the_ending_is_null_then_an_exception_is_thrown()
        {
            var node = new Node<int>(2);

            Assert.Throws<ArgumentNullException>(() => _algorithm.DegreesOfSeparation(node, null));
        }

        [Test]
        public void And_the_starting_node_is_the_same_as_the_finish_node_then_0_is_returned()
        {
            var node = new Node<string>("A");

            const int expectedDegree = 0;

            var result = _algorithm.DegreesOfSeparation(node, node);

            Assert.AreEqual(expectedDegree, result);
        }

        [Test]
        public void And_the_starting_node_is_directly_connected_to_the_finish_node_then_one_is_returned()
        {
            var aNode = new Node<string>("A");
            var bNode = new Node<string>("B");

            aNode.AddNeighbor(bNode);

            const int expectedResult = 1;
            var result = _algorithm.DegreesOfSeparation(aNode, bNode);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void And_the_starting_node_is_separated_by_the_finish_node_by_a_neighbor_then_2_is_returned()
        {
            var aNode = new Node<string>("A");
            var bNode = new Node<string>("B");
            var cNode = new Node<string>("C");

            aNode.AddNeighbor(bNode);
            bNode.AddNeighbor(cNode);

            const int expectedResult = 2;
            var result = _algorithm.DegreesOfSeparation(aNode, cNode);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void And_the_starting_node_is_no_way_connected_to_the_finish_node_then_negative_1_is_returned()
        {
            var aNode = new Node<string>("A");
            var bNode = new Node<string>("B");

            const int expectedResult = -1;
            var result = _algorithm.DegreesOfSeparation(aNode, bNode);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void And_there_is_a_loop_between_the_starting_and_finish_such_that_they_are_not_connected_then_negative_inifinity_is_returned()
        {
            var aNode = new Node<string>("A");
            var bNode = new Node<string>("B");
            var cNode = new Node<string>("C");
            var dNode = new Node<string>("D");
            var eNode = new Node<string>("E");

            aNode.AddNeighbor(bNode);
            bNode.AddNeighbor(cNode);
            cNode.AddNeighbor(dNode);
            cNode.AddNeighbor(aNode);
            dNode.AddNeighbor(cNode);

            var result = _algorithm.DegreesOfSeparation(aNode, eNode);

            Assert.AreEqual(int.MinValue, result);
        }
    }

    // ReSharper restore InconsistentNaming
}
