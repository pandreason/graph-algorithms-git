﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace GraphAlgorithms.Tests.DepthFirstSearch
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    class When_getting_the_path_between_two_nodes
    {
        private GraphAlgorithms.DepthFirstSearch _algorithm;

        [SetUp]
        public void Setup()
        {
            _algorithm = new GraphAlgorithms.DepthFirstSearch();
        }

        [Test]
        public void And_the_starting_node_is_null_then_an_exception_is_thrown()
        {
            var node = new Node<string>("A");

            Assert.Throws<ArgumentNullException>(() => _algorithm.GetPathBetweenNodes(null, node));
        }

        [Test]
        public void And_the_finish_node_is_null_then_an_exception_is_thrown()
        {
            var node = new Node<string>("A");

            Assert.Throws<ArgumentNullException>(() => _algorithm.GetPathBetweenNodes(node, null));
        }

        [Test]
        public void And_the_starting_node_is_the_finish_node_then_the_node_is_returned()
        {
            var node = new Node<string>("A");

            var result = _algorithm.GetPathBetweenNodes(node, node);

            var expectedResult = new List<Node<string>> {node};

            CollectionAssert.AreEquivalent(expectedResult, result);
        }

        [Test]
        public void And_the_starting_node_is_directly_connected_to_the_finishing_node_both_the_starting_and_finishing_nodes_are_returned()
        {
            var startNode = new Node<string>("A");
            var finishNode = new Node<string>("B");

            startNode.AddNeighbor(finishNode);

            var result = _algorithm.GetPathBetweenNodes(startNode, finishNode);

            var expectedResult = new List<Node<string>> { startNode, finishNode};

            CollectionAssert.AreEquivalent(expectedResult, result);
        }

        [Test]
        public void And_the_starting_node_is_not_connected_to_the_finish_node_then_an_empty_list_is_returned()
        {
            var startNode = new Node<string>("A");
            var finishNode = new Node<string>("B");

            var result = _algorithm.GetPathBetweenNodes(startNode, finishNode);

            CollectionAssert.IsEmpty(result);
        }

        [Test]
        public void And_the_starting_node_has_a_cycle_that_does_not_include_the_finish_node_then_an_empty_list_is_returned()
        {
            var startNode = new Node<string>("Start");
            var aNode = new Node<string>("A");
            var bNode = new Node<string>("B");
            var cNode = new Node<string>("C");
            var dNode = new Node<string>("D");
            var finishNode = new Node<string>("Finish");

            startNode.AddNeighbor(aNode);
            aNode.AddNeighbor(bNode);
            bNode.AddNeighbor(cNode);
            cNode.AddNeighbor(aNode);
            cNode.AddNeighbor(dNode);
            dNode.AddNeighbor(bNode);

            var result = _algorithm.GetPathBetweenNodes(startNode, finishNode);

            CollectionAssert.IsEmpty(result);
        }
    }

    // ReSharper restore InconsistentNaming
}
