﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace GraphAlgorithms.Tests.DepthFirstSearch
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    class When_checking_the_graph_for_cycles
    {
        private GraphAlgorithms.DepthFirstSearch _algorithm;

        [SetUp]
        public void Setup()
        {
            _algorithm = new GraphAlgorithms.DepthFirstSearch();
        }

        [Test]
        public void And_the_graph_is_null_then_an_exception_is_thrown()
        {
            Assert.Throws<ArgumentNullException>(() => _algorithm.IsThereACyle<int>(null));
        }

        [Test]
        public void And_the_graph_is_empty_then_false_is_returned()
        {
            var graph = new List<Node<int>>();

            Assert.False(_algorithm.IsThereACyle(graph));
        }

        [Test]
        public void And_the_graph_contains_a_single_node_then_false_is_returned()
        {
            var aNode = new Node<string>("A");
            var graph = new List<Node<string>> {aNode};

            Assert.False(_algorithm.IsThereACyle(graph));
        }

        [Test]
        public void And_the_graph_contains_two_nodes_with_A_connected_to_B_then_false_is_returned()
        {
            var aNode = new Node<string>("A");
            var bNode = new Node<string>("B");

            aNode.AddNeighbor(bNode);

            var graph = new List<Node<string>>{aNode, bNode};

            Assert.False(_algorithm.IsThereACyle(graph));
        }

        [Test]
        public void And_the_graph_contains_two_nodes_with_A_connected_to_B_and_B_connected_to_A_then_false_is_returned()
        {
            var aNode = new Node<string>("A");
            var bNode = new Node<string>("B");

            aNode.AddNeighbor(bNode);
            bNode.AddNeighbor(aNode);

            var graph = new List<Node<string>> {aNode, bNode};

            Assert.IsTrue(_algorithm.IsThereACyle(graph));
        }
    }

    // ReSharper restore InconsistentNaming
}
