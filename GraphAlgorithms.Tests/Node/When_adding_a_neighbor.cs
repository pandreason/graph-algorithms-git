﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace GraphAlgorithms.Tests.Node
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    class When_adding_a_neighbor
    {
        private Node<string> node;

        [SetUp]
        public void Setup()
        {
            node = new Node<string>("A");
        }

        [Test]
        public void And_the_neighbor_is_null_then_an_exception_is_thrown()
        {
            Assert.Throws<ArgumentNullException>(() => node.AddNeighbor(null));
        }

        [Test]
        public void And_the_neighbor_was_not_already_added_then_it_is_added()
        {
            var bNode = new Node<string>("B");
            
            node.AddNeighbor(bNode);

            var expectedNeighbors = new List<Node<string>> {bNode};

            CollectionAssert.AreEquivalent(expectedNeighbors, node.Neighbors);
        }

        [Test]
        public void And_the_neighbor_was_already_added_then_its_not_added_again()
        {
            var bNode = new Node<string>("B");

            node.AddNeighbor(bNode);
            node.AddNeighbor(bNode);
         
            var expectedNeighbors = new List<Node<string>> { bNode };
            CollectionAssert.AreEquivalent(expectedNeighbors, node.Neighbors);
        }
    }

    // ReSharper restore InconsistentNaming
}
