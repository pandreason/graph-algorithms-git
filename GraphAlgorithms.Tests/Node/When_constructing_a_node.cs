﻿using NUnit.Framework;
using System;

namespace GraphAlgorithms.Tests.Node
{
    [TestFixture]
    // ReSharper disable InconsistentNaming
    class When_constructing_a_node
    {
        [Test]
        public void And_the_data_is_null_then_an_exception_is_thrown()
        {
            Assert.Throws<ArgumentNullException>(() => new Node<string>(null));
        }

        [Test]
        public void Then_the_data_is_set_correctly()
        {
            const string data = "SomeString";
            var node = new Node<string>(data);

            Assert.AreEqual(data, node.Data);
        }

        [Test]
        public void Then_the_list_of_neighbors_is_empty()
        {
            var node = new Node<int>(2);

            CollectionAssert.IsEmpty(node.Neighbors);
        }
    }

    // ReSharper restore InconsistentNaming
}
